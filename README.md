# Import and Geocode Evictions

This set of scripts pulls, geocodes, updates, and maintains Massachusetts Eviction Data collected by a team of AmeriCorps Advocates at Community Legal Aid in Springfield, MA.

So far, this is best used as an RScript. To import all rows from all sheets...

```bash
Rscript ./import.R -v -e "<your GSuite account email>" -s "MA"
```

To check for updates, re-geocode as necessary, and reupload:

```bash
Rscript ./update.R -v -e "<your GSuite account email>" -d "<district abbreviation - optional>" -s "MA"
```

Note that you must add the following to your `~/.Renviron` before the scripts will work:

```bash
LL_DB=<database name>
LL_HOST=<database host>
LL_PORT=<database port>
LL_USER=<database role>
LL_PASS=<database password>

E_EVIC=<url of East district sheet>
...
MS_EVIC=<url of Metro South district sheet>

OPENCAGE_KEY=<your OpenCage key>
```